# Training Java Web

### Environment requirement
- Operating System: Ubuntu v18.04
- OpenJDK: 11.x.x
- Maven: 3.x.x
- Mysql: 5.x.x

### Install project
- Install project: `mvn clean install`

### Development (For Developer)
- Run: `mvn spring-boot:run'
### Swagger-ui
- http://localhost:8080/swagger-ui.html#

### Explore Rest APIs
- The app defines following CRUD APIs.

	`GET /xxx`

	`POST /xxx`

	`GET /xxx/{id}`

	`PUT /xxx/{id}`

	`DELETE /xxx/{id}`
- Open the API document at: `http://localhost:8080/swagger-ui.html` (local environment)

You can test them using postman or any other rest client.

