package com.bap.training.java.config;

import com.bap.training.java.constant.Constant;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * LocaleConfig
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Configuration
public class LocaleConfig {

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone(Constant.TIME_ZONE));
    }

}
