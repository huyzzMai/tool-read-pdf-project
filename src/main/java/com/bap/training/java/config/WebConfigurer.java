package com.bap.training.java.config;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ObjectUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.ServletContext;

/**
 * WebConfigurer
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Configuration
@RequiredArgsConstructor
public class WebConfigurer implements ServletContextInitializer {

    private final Logger LOGGER = LoggerFactory.getLogger(WebConfigurer.class);

    private final ApplicationProperties applicationProperties;

    @Override
    public void onStartup(ServletContext servletContext) {
        LOGGER.info("Web application fully configured");
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = applicationProperties.getCors();
        if (!ObjectUtils.isEmpty(config.getAllowedOrigins())) {
            LOGGER.debug("Registering CORS filter");
            source.registerCorsConfiguration("/auth/**", config);
            source.registerCorsConfiguration("/v2/api-docs", config);
        }
        return new CorsFilter(source);
    }

}
