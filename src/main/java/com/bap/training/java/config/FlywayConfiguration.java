package com.bap.training.java.config;

import lombok.RequiredArgsConstructor;
import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * FlywayConfiguration
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Configuration
@ConditionalOnMissingBean(value = org.flywaydb.core.Flyway.class)
@EnableConfigurationProperties(value = {FlywayProperties.class})
@RequiredArgsConstructor
public class FlywayConfiguration {

    private final DataSource dataSource;

    @Bean
    public Flyway flyway(FlywayProperties flywayProperties) {
        Flyway flyway = Flyway.configure()
                .dataSource(dataSource)
                .table(flywayProperties.getTable())
                .baselineOnMigrate(flywayProperties.isBaselineOnMigrate())
                .baselineVersion(flywayProperties.getBaselineVersion())
                .outOfOrder(flywayProperties.isOutOfOrder()).load();
        flyway.repair();
        flyway.migrate();
        return flyway;
    }

}
