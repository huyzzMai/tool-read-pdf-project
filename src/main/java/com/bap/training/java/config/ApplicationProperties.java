package com.bap.training.java.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;

/**
 * ApplicationProperties
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Component
@ConfigurationProperties("application")
@Getter
@Setter
public class ApplicationProperties {

    private final Security security = new Security();
    private final CorsConfiguration cors = new CorsConfiguration();

    @Getter
    @Setter
    public static class Security {
        private String base64Secret;
        private long tokenValidityInSeconds;
    }

}
