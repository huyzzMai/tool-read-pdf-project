package com.bap.training.java.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * API configuration
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Configuration
public class ApiConfiguration implements WebMvcConfigurer {
//    @Autowired
//    private InterceptorConfiguration interceptorConfiguration;
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(interceptorConfiguration);
//    }
}
