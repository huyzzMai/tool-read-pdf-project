package com.bap.training.java.constant;

import org.springframework.stereotype.Component;

/**
 * Constant
 *
 * @author tainv
 * @created 2022-01-12 ― 16:20
 */
@Component
public class Constant {

    public static final String SYSTEM_ACCOUNT = "SYSTEM";
    public static final String PREFIX_ROLE = "ROLE_";
    public static final String TIME_ZONE = "Asia/Tokyo";;

    public static class ApiMessageResponse {

        // Authentication
        public static String accessDenied = "ACCESS_DENIED";
        public static String unauthorized = "UNAUTHORIZED";

        public static String successfully = "SUCCESSFULLY";
        public static String userNotFound = "USER_NOT_FOUND";
        public static String incorrectUsernameOrPassword = "INCORRECT_USERNAME_OR_PASSWORD";
    }

    public static class Role {

        public static Integer STUDENT = 1;
        public static Integer TEACHER = 2;
    }
}
