package com.bap.training.java.utility.api;

import com.bap.training.java.constant.Constant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * ResponseData
 *
 * @author tainv
 * @created 2022-01-12 ― 17:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseData<T> {

    @JsonProperty("message")
    private String message = Constant.ApiMessageResponse.successfully;

    @JsonProperty("status")
    private int status = HttpStatus.OK.value();

    @JsonProperty("data")
    private T data;

    public ResponseData(T data) {
        super();
        this.data = data;
    }

}
