package com.bap.training.java.service;


import com.bap.training.java.dao.entity.User;
import com.bap.training.java.service.dto.authentication.AuthenticationDto;
import com.bap.training.java.service.modelquery.authentication.FormLogin;

/**
 * AuthenticationService
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
public interface AuthenticationService {

    AuthenticationDto login(FormLogin login) throws Exception;

    User getCurrentUserLogin();

}
