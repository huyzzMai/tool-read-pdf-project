package com.bap.training.java.service.impl;

import com.bap.training.java.dao.entity.CustomProvider;
import com.bap.training.java.dao.entity.FilePDF;
import com.bap.training.java.dao.repository.PDFFileRepository;
import com.bap.training.java.service.pdfStorageService.PdfStorageService;
import com.pspdfkit.api.*;
import com.pspdfkit.api.providers.FileDataProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;


@Service
@RequiredArgsConstructor
public class PdfStorageImpl implements PdfStorageService {
    private final String OUTPUTPATH = "tmp\\tmp.txt";
    private final Path root = Paths.get("tmp");
    private final PDFFileRepository pdfFileRepository;
    @Override
    public void init() throws PSPDFKitInitializeException {
        PSPDFKit.initializeTrial();
    }

    @Override
    public void save(MultipartFile multipartFile) throws IOException {
        File saveFile = new File(OUTPUTPATH);

        File file= multipartConvert(multipartFile);
        PdfDocument document = importPDF(file);

        document.saveAs(new FileDataProvider(saveFile), new DocumentSaveOptions.Builder().build());

        FileWriter txtWritter = new FileWriter(OUTPUTPATH);

        StringBuilder pageText = new StringBuilder();
        for (int i = 0; i < document.getPageCount(); i++) {
            List<TextBlock> textLines = document.getPage(i).getTextLines();
            for (TextBlock textLine : textLines) {
                //debug
                pageText.append(textLine.text);
                pageText.append("\r\n");

                //write to txt file
                txtWritter.write(textLine.text);
                txtWritter.write("\r\n");
            }
        }

        //debug log
        System.out.println("The text on the first page reads:\n" + pageText);

        //finish write file
        txtWritter.close();
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public File multipartConvert(MultipartFile multipartFile) throws IOException {
        File convFile = new File( multipartFile.getOriginalFilename() );
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( multipartFile.getBytes() );
        fos.close();
        return convFile;
    }

    @Override
    public PdfDocument importPDF(File file) {
        PdfDocument document = null;
        try {
            init();
            document = PdfDocument.open(new FileDataProvider(file));
        } catch (Exception e){
            e.printStackTrace();
        }
        return document;
    }

    @Override
    public FilePDF store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        FilePDF FileDB = new FilePDF(fileName, file.getContentType(), file.getBytes());
        return pdfFileRepository.save(FileDB);
    }

    @Override
    public FilePDF getFile(Long id) {
        return pdfFileRepository.findById(id).get();
    }

    @Override
    public Stream<FilePDF> getAllFiles() {
        return pdfFileRepository.findAll().stream();
    }

    @Override
    public Resource exportPDF(FilePDF filePDF) {
        Resource resource=null;
        String outputpath = "tmp\\"+filePDF.getName();
        String savefilepath = "tmp\\"+filePDF.getName()+".txt";
        File outputFile=new File(outputpath);
        File saveFile=new File(savefilepath);

        try (FileOutputStream stream = new FileOutputStream(outputFile.getPath(), true)) { //overwrite
            stream.write(filePDF.getData());

            PdfDocument pdfDocument=importPDF(outputFile);

            pdfDocument.saveAs(new FileDataProvider(saveFile), new DocumentSaveOptions.Builder().build());
            FileWriter txtWritter = new FileWriter(savefilepath);

            for (int i = 0; i < pdfDocument.getPageCount(); i++) {
                List<TextBlock> textLines = pdfDocument.getPage(i).getTextLines();
                for (TextBlock textLine : textLines) {
                    txtWritter.write(textLine.text);
                    txtWritter.write("\r\n");
                }
            }
            txtWritter.close();

            resource=load(filePDF.getName()+".txt");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return resource;
    }
}
