package com.bap.training.java.service.modelquery.authentication;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * FormLogin
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FormLogin {

    @NotBlank
    private String userName;

    @NotBlank
    private String passWord;

    @NotNull
    private Integer role;
}
