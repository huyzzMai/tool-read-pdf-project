package com.bap.training.java.service.impl;

import com.bap.training.java.constant.Constant;
import com.bap.training.java.dao.entity.User;
import com.bap.training.java.dao.repository.UserRepository;
import com.bap.training.java.security.SecurityUtil;
import com.bap.training.java.security.jwt.TokenProvider;
import com.bap.training.java.service.AuthenticationService;
import com.bap.training.java.service.dto.authentication.AuthenticationDto;
import com.bap.training.java.service.modelquery.authentication.FormLogin;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import java.util.Optional;

/**
 * AuthenticationServiceImpl
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Service
@Transactional
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    private final TokenProvider tokenProvider;

    private final UserRepository userRepository;

    @Override
    public AuthenticationDto login(FormLogin login) {

        User user = userRepository.findFirstByUsernameAndPasswordAndRoleAndDeletedFalse(
                login.getUserName(), login.getPassWord(), login.getRole()
        ).orElseThrow(() -> new BadRequestException(Constant.ApiMessageResponse.incorrectUsernameOrPassword));

        return AuthenticationDto.builder()
                .token(tokenProvider.createToken(user))
                .build();
    }

    @Override
    public User getCurrentUserLogin() {
        Optional<String> userIdOpt = SecurityUtil.getCurrentUserLogin();
        if (userIdOpt.isPresent()) {
            return userRepository.findOneByIdAndDeletedFalse(Long.valueOf(userIdOpt.get()))
                    .orElseThrow(() -> new BadRequestException(Constant.ApiMessageResponse.userNotFound));
        } else {
            throw new BadRequestException(Constant.ApiMessageResponse.userNotFound);
        }
    }

}
