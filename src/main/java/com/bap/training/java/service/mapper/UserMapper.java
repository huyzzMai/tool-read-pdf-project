package com.bap.training.java.service.mapper;

import com.bap.training.java.dao.entity.User;
import com.bap.training.java.service.modelquery.authentication.FormLogin;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * UserMapper
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "userName", target = "username")
    @Mapping(source = "passWord", target = "password")
    @Mapping(source = "role", target = "role")
    User toEntity(FormLogin loginDto);

}
