package com.bap.training.java.service.dto.authentication;

import lombok.Builder;
import lombok.Data;

/**
 * AuthenticationDto
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Data
@Builder
public class AuthenticationDto {

    private String token;

}
