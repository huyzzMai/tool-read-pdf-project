package com.bap.training.java.service.pdfStorageService;

import com.bap.training.java.dao.entity.FilePDF;
import com.pspdfkit.api.PSPDFKitInitializeException;
import com.pspdfkit.api.PdfDocument;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

public interface PdfStorageService {
    public void init() throws PSPDFKitInitializeException;
    public void save(MultipartFile file) throws IOException;
    public Resource load(String filename);
    File multipartConvert(MultipartFile multipartFile) throws IOException;
    PdfDocument importPDF(File file);
    public FilePDF store(MultipartFile file) throws IOException;
    public FilePDF getFile(Long id);
    public Stream<FilePDF> getAllFiles();

    public Resource exportPDF(FilePDF filePDF);
}
