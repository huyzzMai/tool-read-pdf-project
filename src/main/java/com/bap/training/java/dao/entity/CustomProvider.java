package com.bap.training.java.dao.entity;

import com.pspdfkit.api.providers.InputStreamDataProvider;
import com.pspdfkit.api.providers.WritableDataProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CustomProvider extends InputStreamDataProvider implements WritableDataProvider {
    private List<Byte> pdfData = new ArrayList<>();

    public CustomProvider(@NotNull final byte[] data) {
        for (byte value : data) {
            pdfData.add(value);
        }
    }

    @NotNull
    @Override
    protected InputStream openInputStream() {
        byte[] data = new byte[pdfData.size()];
        for (int i = 0; i < pdfData.size(); i++) {
            data[i] = pdfData.get(i);
        }
        return new ByteArrayInputStream(data);
    }

    @Override
    public long getSize() {
        return pdfData.size();
    }

    @Nullable
    @Override
    public String getTitle() {
        return "Fake-Title";
    }

    @Override
    public boolean canWrite() {
        // You're allowing writing.
        return true;
    }

    @Override
    public boolean startWrite(WriteMode writeMode) {
        // Remove the current data if you're rewriting from scratch.
        if (writeMode == WriteMode.REWRITE) {
            pdfData.clear();
        }

        return true;
    }

    @Override
    public boolean write(byte[] bytes) {
        for (byte value : bytes) {
            pdfData.add(value);
        }
        return true;
    }

    @Override
    public boolean finishWrite() {
        // Nothing to do.
        return true;
    }

    @Override
    public boolean supportsAppending() {
        // You can append onto the string if you want to.
        return true;
    }
}
