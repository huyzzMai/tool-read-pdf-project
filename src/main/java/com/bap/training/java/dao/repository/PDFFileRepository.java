package com.bap.training.java.dao.repository;

import com.bap.training.java.dao.entity.FilePDF;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PDFFileRepository extends JpaRepository<FilePDF, Long> {

    Optional<FilePDF> findByIdAndDeleteIsFalse(Long fileId);
}
