package com.bap.training.java.dao.repository;

import com.bap.training.java.dao.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * UserRepository
 *
 * @author tainv
 * @created 2022-01-12 ― 17:13
 */
@Repository
public interface UserRepository  extends JpaRepository<User, Long> {

    Optional<User> findOneByIdAndDeletedFalse(Long userId);

    Optional<User> findFirstByUsernameAndPasswordAndRoleAndDeletedFalse(String username, String passWord, Integer role);
}
