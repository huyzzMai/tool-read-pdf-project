package com.bap.training.java.dao.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * AbstractAuditingEntity
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@MappedSuperclass
@Audited
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public abstract class AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "deleted", nullable = false, columnDefinition = "bit default false")
    private boolean deleted = false;

    @CreatedBy
    @Column(name = "created_by", nullable = false,
        columnDefinition = "varchar(" + 255 + ") default '" + "SYSTEM" +"'",
        length = 255, updatable = false)
    private String createdBy;

    @LastModifiedBy
    @Column(name = "last_modified_by", length = 255)
    private String lastModifiedBy;

    @Column(name = "created_date", updatable = false,
        columnDefinition = "DATETIME(" + 255 + ") DEFAULT CURRENT_TIMESTAMP",
        length = 255)
    @CreatedDate
    private Timestamp createdDate;

    @Column(name = "last_modified_date")
    @LastModifiedDate
    private Timestamp lastModifiedDate;

}
