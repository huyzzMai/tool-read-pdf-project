package com.bap.training.java.dao.entity;

import lombok.*;

import javax.persistence.*;

@Builder
@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "files")
public class FilePDF {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "file_name")
    private String name;

    @Column(name = "type")
    private String type;

    @Lob
    @Column(name = "data", columnDefinition = "LONGBLOB")
    private byte[] data;

    @Column(name ="deleted")
    private Boolean delete;

    public FilePDF (String name, String type, byte[] data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }
}
