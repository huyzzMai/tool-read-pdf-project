package com.bap.training.java.security;

import com.bap.training.java.constant.Constant;
import com.bap.training.java.dao.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * MyUserDetails
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@NoArgsConstructor
@Getter
@Setter
public class MyUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    MyUserDetails(User user) {
        super();
        this.username = String.valueOf(user.getId());
        this.password = "";
        this.authorities = Collections.singletonList(
                new SimpleGrantedAuthority(Constant.PREFIX_ROLE + user.getRole().toString())
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
