package com.bap.training.java.security;

import com.bap.training.java.constant.Constant;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * SpringSecurityAuditorAware
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(SecurityUtil.getCurrentUserLogin().orElse(Constant.SYSTEM_ACCOUNT));
    }

}
