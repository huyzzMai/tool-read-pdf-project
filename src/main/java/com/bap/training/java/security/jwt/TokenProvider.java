package com.bap.training.java.security.jwt;

import com.bap.training.java.config.ApplicationProperties;
import com.bap.training.java.dao.entity.User;
import com.bap.training.java.security.MyUserDetailsService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.Key;
import java.util.Date;

/**
 * TokenProvider
 *
 * @author tainv
 * @created 2022-01-12 ― 17:13
 */
@Component
@RequiredArgsConstructor
public class TokenProvider implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(TokenProvider.class);

    private Key key;
    private long tokenValidityInMilliseconds;
    private static final String AUTHORIZATION_HEADER = "Authorization";

    private final ApplicationProperties applicationProperties;
    private final MyUserDetailsService myUserDetailsService;

    /**
     * Init decode key base64 secret to key
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    @PostConstruct
    protected void init() {
        byte[] keyBytes = Decoders.BASE64.decode(applicationProperties.getSecurity().getBase64Secret());
        this.key = Keys.hmacShaKeyFor(keyBytes);
        this.tokenValidityInMilliseconds = 1000 * applicationProperties.getSecurity().getTokenValidityInSeconds();
    }

    /**
     * Create token
     *
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    public String createToken(User user) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + tokenValidityInMilliseconds);
        return Jwts.builder()
                   .setSubject(String.valueOf(user.getId()))
                   .setIssuedAt(now)
                   .setExpiration(expiryDate)
                   .signWith(key, SignatureAlgorithm.HS512)
                   .compact();
    }

    /**
     * Get authentication
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    Authentication getAuthentication(String token) {
        UserDetails userDetails = myUserDetailsService.loadUserByUsername(getUsernameByToken(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    /**
     * Get username by token
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    private String getUsernameByToken(String token) {
        return Jwts.parserBuilder()
                   .setSigningKey(key)
                   .build()
                   .parseClaimsJws(token)
                   .getBody()
                   .getSubject();
    }

    /**
     * Validate token
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    public boolean validateToken(String token) {
        try {
            Jws<Claims> jws = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);
            return isTokenNonExpired(jws.getBody().getExpiration());
        } catch (MalformedJwtException | IllegalArgumentException ex) {
            LOG.error("Invalid JWT token or JWT claims string is empty.");
        }
        return false;
    }

    /**
     * Check token non expired
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    private boolean isTokenNonExpired(Date expiration) {
        return expiration.after(new Date());
    }

    /**
     * Resolve token
     *
     * @author tainv
     * @created 2022-01-12 ― 17:13
     */
    String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);
        return StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")
                ? bearerToken.substring(7) : null;
    }

}
