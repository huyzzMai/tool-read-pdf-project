package com.bap.training.java.security;

import com.bap.training.java.dao.entity.User;
import com.bap.training.java.dao.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * MyUserDetailsService
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@Service
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findOneByIdAndDeletedFalse(Long.valueOf(username))
                .orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + username));
        return new MyUserDetails(user);
    }

}
