package com.bap.training.java.exception;

import com.bap.training.java.constant.Constant;
import com.bap.training.java.utility.api.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * GlobalExceptionHandler
 *
 * @author thanhd
 * @since 1.0
 * @created 10/05/2021 14:00:00
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Handle bad request exception
     *
     * @author thanhd
     * @update thanhd
     * @lastModifier 29/05/2021 14:00:00
     * @param ex BadRequestException
     * @return ResponseEntity<ResponseData<Object>>
     */
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ResponseData<Object>> handleBadRequestException(BadRequestException ex) {
        ResponseData<Object> customError = ResponseData.builder()
                .message(ex.getMessage())
                .status(HttpStatus.BAD_REQUEST.value()).build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(customError);
    }

    /**
     * Handle access denied exception
     *
     * @author thanhd
     * @update thanhd
     * @lastModifier 29/05/2021 14:00:00
     * @return ResponseEntity<ResponseData<Object>>
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ResponseData<Object>> handleAccessDeniedException() {
        ResponseData<Object> customError = ResponseData.builder()
                .message(Constant.ApiMessageResponse.accessDenied)
                .status(HttpStatus.BAD_REQUEST.value()).build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(customError);
    }

    /**
     * Handle insufficient exception
     *
     * @author thanhd
     * @update thanhd
     * @lastModifier 31/05/2021 14:00:00
     * @return ResponseEntity<ResponseData<Object>>
     */
    @ExceptionHandler({InsufficientAuthenticationException.class})
    public ResponseEntity<ResponseData<Object>> handleUnAuthorizeException() {
        ResponseData<Object> customError = ResponseData.builder()
                .message(Constant.ApiMessageResponse.unauthorized)
                .status(HttpStatus.BAD_REQUEST.value()).build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(customError);
    }

    /**
     * Handle internal server error exception
     *
     * @author thanhd
     * @update thanhd
     * @lastModifier 29/05/2021 14:00:00
     * @param ex Exception
     * @return ResponseEntity<ResponseData<Object>>
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseData<Object>> handleInternalServerErrorException(Exception ex) {
        ex.printStackTrace();
        ResponseData<Object> customError = ResponseData.builder()
                .message(ex.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value()).build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(customError);
    }

    /**
     * Handle service unavailable
     *
     * @param ex ServiceUnavailableException
     * @return ResponseEntity<ResponseData<Object>>
     */
    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<ResponseData<Object>> handleServiceUnavailableException(ServiceUnavailableException ex) {
        ResponseData<Object> customError = ResponseData.builder()
                .message(ex.getMessage())
                .status(HttpStatus.SERVICE_UNAVAILABLE.value()).build();
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(customError);
    }
}
