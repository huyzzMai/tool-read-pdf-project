package com.bap.training.java.exception;

/**
 * BadRequestException
 *
 * @author thanhd
 * @since 1.0
 * @created 29/05/2021 14:00:00
 */
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BadRequestException(String message) {
        super(message);
    }

}
