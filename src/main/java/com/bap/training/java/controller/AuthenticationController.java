package com.bap.training.java.controller;

import com.bap.training.java.service.AuthenticationService;
import com.bap.training.java.service.modelquery.authentication.FormLogin;
import com.bap.training.java.utility.api.ResponseData;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AuthenticationController
 *
 * @author tainv
 * @created 2022-01-12 ― 16:18
 */
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping(value = "/login")
    public ResponseEntity<ResponseData> login(@Validated @RequestBody FormLogin login) throws Exception {
        return ResponseEntity.ok().body(new ResponseData<>(authenticationService.login(login)));
    }

}
