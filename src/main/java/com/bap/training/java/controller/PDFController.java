package com.bap.training.java.controller;

import com.bap.training.java.dao.entity.CustomProvider;
import com.bap.training.java.dao.entity.FilePDF;
import com.bap.training.java.service.AuthenticationService;
import com.bap.training.java.service.pdfStorageService.PdfStorageService;
import com.bap.training.java.utility.api.ResponseData;
import com.pspdfkit.api.PSPDFKitInitializeException;
import com.pspdfkit.api.PdfDocument;
import lombok.RequiredArgsConstructor;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequestMapping("/pdf")
@RequiredArgsConstructor
public class PDFController {
    private final PdfStorageService pdfStorageService;
    private final String OUTPUTFILENAME="tmp.txt";

    @PostMapping(value = "/upload")
    public ResponseEntity<Resource> readFilePDF(@RequestParam("uploadFile") MultipartFile multipartFile) throws IOException {
        pdfStorageService.save(multipartFile);
        pdfStorageService.store(multipartFile);
        return ResponseEntity.ok().body(pdfStorageService.load(OUTPUTFILENAME));
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<?> getFile(@PathVariable Long id) {
        FilePDF filePDF = pdfStorageService.getFile(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filePDF.getName() + "\"")
                .body(pdfStorageService.exportPDF(filePDF));
    }
}
