CREATE TABLE IF NOT EXISTS users
(
    id                 INT(19) AUTO_INCREMENT,
    username           VARCHAR (255) NOT NULL,
    password           VARCHAR(255) NOT NULL,
    role               INT(2) NOT NULL,
    deleted            INT(1) DEFAULT 0,
    created_by         VARCHAR(255) DEFAULT 'SYSTEM',
    last_modified_by   VARCHAR(255),
    created_date       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
