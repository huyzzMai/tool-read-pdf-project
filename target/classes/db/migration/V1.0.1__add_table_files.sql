CREATE TABLE IF NOT EXISTS files
(
    id                 INT(19) AUTO_INCREMENT,
    file_name          VARCHAR (255) NOT NULL,
    type               VARCHAR (255) NOT NULL,
    data               LONGBLOB,
    deleted            INT(1) DEFAULT 0,
    created_by         VARCHAR(255) DEFAULT 'SYSTEM',
    last_modified_by   VARCHAR(255),
    created_date       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
