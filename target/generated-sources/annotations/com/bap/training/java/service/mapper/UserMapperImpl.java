package com.bap.training.java.service.mapper;

import com.bap.training.java.dao.entity.User;
import com.bap.training.java.service.modelquery.authentication.FormLogin;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-02-14T18:10:34+0700",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.13 (Amazon.com Inc.)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public User toEntity(FormLogin loginDto) {
        if ( loginDto == null ) {
            return null;
        }

        User user = new User();

        user.setPassword( loginDto.getPassWord() );
        user.setRole( loginDto.getRole() );
        user.setUsername( loginDto.getUserName() );

        return user;
    }
}
